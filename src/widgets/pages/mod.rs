mod buttons;
mod double_image;
mod image;
mod image_common;
mod welcome;

pub use buttons::ButtonsWidget;
pub use double_image::DoubleImagePageWidget;
pub use image::ImagePageWidget;
pub use welcome::WelcomePageWidget;
