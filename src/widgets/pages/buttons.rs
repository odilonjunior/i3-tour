use crate::utils::i18n_f;
use crate::widgets::pages::image_common::add_img_box;
use gettextrs::gettext;
use gtk::prelude::*;

pub struct ButtonsWidget {
    pub widget: gtk::Box,
}

impl<'a> ButtonsWidget {
    pub fn new(modifier: &str, has_liveinst: bool) -> Self {
        let widget = gtk::Box::new(gtk::Orientation::Vertical, 0);

        let image_page = Self { widget };

        image_page.init(modifier, has_liveinst);
        image_page
    }

    fn init(&self, modifier: &str, has_liveinst: bool) {
        self.widget.set_property_expand(true);
        self.widget.get_style_context().add_class("page");
        self.widget.set_halign(gtk::Align::Fill);
        self.widget.set_valign(gtk::Align::Fill);

        let container = gtk::BoxBuilder::new()
            .orientation(gtk::Orientation::Vertical)
            .spacing(12)
            .halign(gtk::Align::Center)
            .valign(gtk::Align::Center)
            .vexpand(true)
            .margin_bottom(48)
            .margin_top(12)
            .margin_start(12)
            .margin_end(12)
            .build();

        add_img_box(
            &container,
            "/org/i3/Tour/i3-logo.svg",
            "That's all folks!".to_string(),
            "Choose your next steps".to_string(),
        );

        let launch_liveinst_btn = gtk::Button::with_label(
            &(if has_liveinst {
                gettext("Launch installer")
            } else {
                gettext("Launch installer (not present on your system)")
            }),
        );
        launch_liveinst_btn
            .get_style_context()
            .add_class("suggested-action");
        launch_liveinst_btn.set_action_name(Some("app.launch-liveinst"));
        launch_liveinst_btn.set_visible(true);
        launch_liveinst_btn.show();

        container.add(&launch_liveinst_btn);

        let open_config_btn = gtk::Button::with_label(&gettext("Edit your i3 config"));
        open_config_btn
            .get_style_context()
            .add_class("suggested-action");
        open_config_btn.set_action_name(Some("app.open-config"));
        open_config_btn.set_visible(true);
        open_config_btn.show();

        let open_i3_userguide_btn = gtk::Button::with_label(&gettext("Open i3 User Guide"));
        open_i3_userguide_btn
            .get_style_context()
            .add_class("suggested-action");
        open_i3_userguide_btn.set_action_name(Some("app.open-i3-userguide"));
        open_i3_userguide_btn.set_visible(true);
        open_i3_userguide_btn.show();

        container.add(&open_config_btn);
        container.add(&open_i3_userguide_btn);

        container.show();

        let below_label = gtk::LabelBuilder::new()
            .label(&i18n_f(
                "You can relaunch this application anytime via {}+d and entering i3-tour",
                &[&modifier],
            ))
            .lines(2)
            .wrap(true)
            .justify(gtk::Justification::Center)
            .valign(gtk::Align::Center)
            .margin_top(12)
            .build();
        below_label.get_style_context().add_class("page-body");
        below_label.show();
        container.add(&below_label);

        self.widget.add(&container);
        self.widget.show();
    }
}
