#[cfg(feature = "video")]
use crate::config;
use crate::utils::i18n_f;
use gettextrs::gettext;
use gtk::prelude::*;

pub struct WelcomePageWidget {
    pub widget: gtk::Box,
}

impl WelcomePageWidget {
    pub fn new(modifier: &str) -> Self {
        let widget = gtk::Box::new(gtk::Orientation::Horizontal, 0);

        let welcome_page = Self { widget };

        welcome_page.init(modifier);
        welcome_page
    }

    fn init(&self, modifier: &str) {
        let container = gtk::BoxBuilder::new()
            .orientation(gtk::Orientation::Vertical)
            .spacing(0)
            .expand(true)
            .valign(gtk::Align::Center)
            .halign(gtk::Align::Center)
            .margin_top(24)
            .margin_bottom(24)
            .build();
        self.widget.get_style_context().add_class("page");
        self.widget.get_style_context().add_class("welcome-page");

        let header = {
            let logo = gtk::Image::from_resource("/org/i3/Tour/Fedora_i3_Logo.svg");
            logo.show();

            logo.upcast::<gtk::Widget>()
        };

        container.add(&header);
        let title = gtk::Label::new(Some(&gettext("Welcome to i3")));
        title.set_margin_top(36);
        title.get_style_context().add_class("large-title");
        title.show();
        container.add(&title);

        for txt in [
            &i18n_f("i3 is a keyboard driven, tilling window manager. Most actions are triggered by pressing the modifier key (yours is currently set to {}) and additional keys.", &[modifier]),
            &gettext("This application will show you the default keybindings adjusted to your current modifier key.")
        ] {
            let text = gtk::Label::new(Some(txt));
            text.get_style_context().add_class("body");
            text.set_margin_top(12);
            text.show();
            container.add(&text);
        }

        container.show();
        self.widget.add(&container);
        self.widget.show();
    }
}
