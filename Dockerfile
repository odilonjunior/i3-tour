FROM fedora:34
RUN dnf install -y gdk-pixbuf2-devel rust meson cargo glib2-devel gtk3-devel.x86_64 libhandy-devel \
    && dnf clean all
WORKDIR /usr/src/myapp
COPY . .
RUN meson build && meson compile -C build

FROM fedora:34
COPY --from=0  /usr/src/myapp/build/src/ /srv/
